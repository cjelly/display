# Purpose

This repo documents steps to run a full-screen Compose Desktop app on Raspberry Pi. 

# Setup instructions

## (1) Flash base image

> 💡 Reference: https://www.jeffgeerling.com/blog/2020/how-flash-raspberry-pi-os-compute-module-4-emmc-usbboot

Flash Raspberry Pi OS Lite (64-Bit) [TODO @cjelly  Specify the commit hash] onto a Pi 4 or CM4 using Raspberry Pi Imager. Use the settings menu to enable SSH and add a public key for the `pi` user.

## (2) Dependencies

### (2.1) Install dependencies

Install dependencies including Podman (as [Docker](https://developers.redhat.com/blog/2020/11/19/transitioning-from-docker-to-podman)), X Window System components, and other system utilities.

```
sudo apt -y update
sudo apt install -y --no-install-recommends \
    slirp4netns \
    git \
    podman \
    uidmap \
    xserver-xorg \
    xinit \
    x11-xserver-utils \
    x11-apps \
    matchbox-window-manager \
    xautomation \
    unclutter
```

### (2.2) Configure Dependencies

Add the default Docker Hub registry to Podman's list of registries.

```
echo "
[registries.search]
registries = ['docker.io']" | \
    sudo tee -a /etc/containers/registries.conf
```

## (3) Set how the Pi behaves after reboot / power cycle

### (3.1) Enable automatic login for the default user

> 💡 Reference: https://raspberrypi.stackexchange.com/questions/40415/how-to-enable-auto-login

Use `raspi-config` to enable "Console Autologin" for the `pi` user.

### (3.2) Disable the Pi screen that asks for new credentials

> 💡 Reference: https://forums.raspberrypi.com/viewtopic.php?t=339340

This will appear by default on a connected display when the OS boots. Disable the screen with the commands below:

```
sudo systemctl disable userconfig > /dev/null 2&>1
sudo systemctl enable getty@tty1
```

### (3.3) Specify launch behavior

Add the following to `/home/pi/display`:

```
#!/bin/sh
xset -dpms
xset s off
xset s noblank
matchbox-window-manager -use_titlebar no &
unclutter &
xeyes
```

Update `/home/pi/.bashrc` by running:

```aidl
echo "xinit /home/pi/display -- vt1" >> ~/.bashrc
```

(Note: you may need to use `vt$(fgconsole)` instead of the specific reference to `vt1` above)

### (3.4) Reboot

Use `sudo reboot` to reboot the Pi. After the above steps, a display connected to the Pi's HDMI port should boot into the `pi` user's shell, and then to the xeyes GUI application   

## (4) Try to run a Dockerized GUI app

[TODO @cjelly : provide steps for how to run a Dockerized app that uses Compose Desktop - e.g. see the code in this repo]

# References

* https://reelyactive.github.io/diy/pi-kiosk/