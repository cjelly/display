import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.awt.SwingPanel
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.WindowPlacement
import androidx.compose.ui.window.WindowState
import androidx.compose.ui.window.application
import com.teamdev.jxbrowser.engine.Engine
import com.teamdev.jxbrowser.engine.EngineOptions
import com.teamdev.jxbrowser.engine.RenderingMode
import com.teamdev.jxbrowser.view.swing.BrowserView
import kotlinx.coroutines.delay
import javax.swing.BoxLayout
import javax.swing.JPanel

fun getEnvVar(key: String)= System.getenv(key)

@Composable
fun WebView(url: String) {
  val engine = Engine.newInstance(
    EngineOptions
      .newBuilder(RenderingMode.HARDWARE_ACCELERATED)
      .licenseKey(getEnvVar("JXBROWSER_KEY"))
      .build()
  )
  val browser = engine.newBrowser()

  browser.navigation().loadUrl(url)

  SwingPanel(factory = {
    JPanel().apply {
      layout = BoxLayout(this, BoxLayout.Y_AXIS)
      add(BrowserView.newInstance(browser))
    }
  })
}


@Composable
@Preview
fun UrlRotater() {
  MaterialTheme {
    Box(modifier = Modifier.fillMaxSize()) {
      WebView("https://trello.com")
    }
  }
}

fun main() = application {
  Window(onCloseRequest = ::exitApplication, state = WindowState(placement = WindowPlacement.Fullscreen)) {
    UrlRotater()
  }
}
